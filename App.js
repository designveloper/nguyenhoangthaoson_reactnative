import React, { Component } from 'react';
import {
 View,
 StyleSheet
} from 'react-native';
import {
  StackNavigator,
  
} from 'react-navigation';

import Login from './LoginForm/Login'
import ListViewClass from './ListViewClass'


export default class App extends Component {  

  static navigationOptions = {
    title: 'Login Screen'
  }
  constructor(props){
    super(props)
    
  }
  render() {
    
     const { navigate } = this.props.navigation
     
    return (
      <View style={styles.container}>
          <Login value={navigate}/>
      </View>
    )
  }
  
}


const styles = StyleSheet.create({ 
  container: {
    flex:1,
    backgroundColor: '#42f1f4',
    justifyContent: 'center'
  },
  logoImage: {
    width: 100,
    height:100
  },
  logo: {
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center'
  },
  title:{
    color: 'black',
    marginTop: 10,
    width: 200,
    textAlign: 'center',
    opacity: 0.9
  }
});