import { AppRegistry } from 'react-native';
import App from './App';
import { StackNavigator } from 'react-navigation'
import Login from './LoginForm/Login'
import ListViewClass from './ListViewClass'


const Link = StackNavigator({
    Home: {screen: App},
    ColorList: {screen: ListViewClass}
})


  

AppRegistry.registerComponent('MyApp', () => Link);
