/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  AppRegistry,
  Image,
  Dimensions,
  Button,
  ListView,
  ScrollView
} from 'react-native';
import ColorButton from './ColorButton'

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class ListViewClass extends Component {  
  static navigationOption = {
    title: 'Color List'
  }
  constructor() {
    super()

    this.ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
    const availableColors = [
      'red',
      'green',
      'yellow',
      'salmon',
      'pink',
      '#0000FF',
      'rgba(255,0,255,.9)',
      'red',
      'green',
      'yellow',
      'salmon',
      'pink',
      '#0000FF',
      'rgba(255,0,255,.9)'
    ]
    this.state = {
      backgroundColor: 'blue',
      availableColors,
      dataSource: this.ds.cloneWithRows(availableColors)
    }
    this.changeColor = this.changeColor.bind(this)
  }

  changeColor(backgroundColor) {
    this.setState({backgroundColor})
  }

  render() {
    const { backgroundColor, dataSource } = this.state;
    return (
      <ListView style={[styles.container,{backgroundColor}]}
        dataSource={dataSource}
        renderRow={(color) => (
          <ColorButton backgroundColor={color}
            onSelect={this.changeColor}/>
        )}
        renderHeader={() => (
          <Text style={styles.header}>Color List</Text>
        )}>

      </ListView>
    )
  }
}


const styles = StyleSheet.create({ 


  container: {
    flex: 1
  },
  header: {
    backgroundColor: 'lightgrey',
    padding: 10,
    paddingTop: 20,
    fontSize: 30,
    textAlign: 'center'
  }
});
