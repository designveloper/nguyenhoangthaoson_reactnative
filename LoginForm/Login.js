/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  AppRegistry,
  Image,
  Dimensions,
  Button,
  ListView,
  ScrollView,
  KeyboardAvoidingView
} from 'react-native';
import ColorButton from '../ColorButton'
import LoginForm from './LoginForm'
import FBLogo from '../images/facebook-logo-new.png'

export default class Login extends Component {  

  

  constructor(props){
    super(props)
    
  }
  render() {
    const value = this.props.value;
   
    return (
       
        <KeyboardAvoidingView behavior="padding" style={styles.container}>            
            <View style={styles.logo}>
                <Image source={FBLogo} style={styles.logoImage}/>
                <Text style={styles.title }>Login Screen</Text>
            </View>
            <View style={styles.form}>
                <LoginForm value={value} />
            </View>            
        </KeyboardAvoidingView>
        
    )
  }
}


const styles = StyleSheet.create({ 
  container: {
    marginTop:40
  },
  logoImage: {
    width: 100,
    height:100
  },
  logo: {
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center'
  },
  title:{
    color: 'black',
    marginTop: 10,
    width: 200,
    textAlign: 'center',
    opacity: 0.9
  }
});
