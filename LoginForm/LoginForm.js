/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';

import {
 View,
 TextView,
 StyleSheet,
 TextInput,
 TouchableOpacity,
 Text,
 StatusBar
} from 'react-native';

  

import ListViewClass from '../ListViewClass'


export default class LoginForm extends Component {  

    
    constructor(props){
        super(props)
        
        this.checkIn = this.checkIn.bind(this);
        this.state = {
           usernameInput: '',
           passwordInput: '' 
        }
        
    }

    checkIn(){
        
       let username = this.state.usernameInput;
       let password = this.state.passwordInput;
       // console.log(username+ " "+password)
       //console.log(this.state.usernameInput)
       var navigate = this.props.value;
       console.log(navigate)
       if (username === "admin" && password==="123"){
           navigate('ColorList')
       }
        
    }

    render() {

        return (
        <View style={styles.container}>   
            <StatusBar barStyle="light-content"/>
            <TextInput style={styles.input} placeholder="Username" 
            placeholderTextColor="red" returnKeyType="next"
            onSubmitEditing={()=>
            this.state.passwordInput.focus()   
            }
            onChangeText = {(input)=> this.setState({
                usernameInput: input
            })}
            />
            <TextInput style={styles.input} placeholder="Password"
            secureTextEntry placeholderTextColor="red"
            autoCapitalize="none" autoCorrect={false}
            returnKeyType="go" onChangeText={(input)=>this.setState({
                passwordInput:input
            })}
            />
            <TouchableOpacity style={styles.buttonContain} onPress={()=>this.checkIn()}>
                <Text style={styles.buttonText}>
                    Login 
                </Text>
            </TouchableOpacity>
        </View>
        )
    }
}


const styles = StyleSheet.create({ 
  container: {
    padding:20

  },
  input: {
      height:40,
      backgroundColor: 'yellow',
      alignItems:'center'

  },
  buttonText:{
    color: 'white',
    textAlign: 'center',
    fontWeight: '400'
  },
  buttonContain: {
    backgroundColor: 'green',
    padding:10
  }
});
